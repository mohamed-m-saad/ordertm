

#include <iostream>
using namespace std;

#include "../conf.h"

#include "../algs/commons.hpp"

int MAIN_HASH(int argc, char *argv[]) {
	INFO("Hi");
	commons::init_rand();
	commons::stick_worker(1);

	int size = 30;
	int iterations = 100;
	int elements_size = 10000;

	long *elements = new long[elements_size];

	int generated[iterations][size];

	for(int n=0; n<iterations; n++){
		std::cout << "N" << n << ":\t";
		for(int i=0; i<size; i++){
			generated[n][i] = (long)(&(elements[commons::rand_r_32() % elements_size]))%2048;
			std::cout << generated[n][i] << "\t";
		}
		std::cout << std::endl;
	}

	int found = 0;
	for(int n=0; n<iterations; n++)
		for(int m=0; m<iterations; m++)
			if(n!=m){
				bool intersect = false;
				int val;
				for(int i=0; i<size; i++)
					for(int j=0; j<size; j++)
						if(generated[n][i] == generated[m][i]){
							intersect = true;
							val = generated[n][i];
						}
				if(intersect){
					if(m-n < 3 && m-n > 0 || n-m < 4 && n-m > 0)
						found++;
					std::cout << n << " " << m << " @ " << val << std::endl;
				}
			}


	INFO("Bye " << (float)(found/2)/iterations);
	return 0;
}
