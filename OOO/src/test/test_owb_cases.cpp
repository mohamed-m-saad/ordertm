
#include <iostream>
using namespace std;

#include "../conf.h"

#include "../algs/owb.hpp"
using namespace owb;

void testSeq(bool abort1, bool abort2) {
	Transaction tx1(1);
	Transaction tx2(2);

	long v1 = 100;
	long v2 = 200;
	tx1.write_l(&v1, 80);
	tx1.write_l(&v2, 90);
	if (abort1)
		tx1.abort(0);
	else {
		tx1.commit();
		tx1.complete();
	}

	long v3 = tx2.read_l(&v1);
	long v4 = tx2.read_l(&v2);
	long v5 = 0;
	tx2.write_l(&v5, v3 + v4);
	if (abort2)
		tx2.abort(0);
	else {
		tx2.commit();
		tx2.complete();
	}

	cout << "S: Tx1=" << abort1 << " Tx2=" << abort2 << " [O1=" << v1
			<< ",\tO2=" << v2 << ",\tO3=" << v3 << ",\tO4=" << v4 << ",\tO5="
			<< v5 << "\t]" << endl;
}

void caseA() {
	long o = 100;

	Transaction tx1(1);
	Transaction tx2(2);

	long v1 = tx1.read_l(&o);
	tx1.commit();

	long v2 = tx2.read_l(&o);
	tx2.commit();

	tx1.complete();
	tx2.complete();

	cout << "A: Tx1=" << tx1.getStatus() << " Tx2=" << tx2.getStatus() << " [O="
			<< o << ",\tV1=" << v1 << ",\tV2=" << v2 << "\t]" << endl;
}

void caseB() {
	long o = 100;

	Transaction tx1(1);
	Transaction tx2(2);

	long v2 = tx2.read_l(&o);
	tx1.commit();

	long v1 = tx1.read_l(&o);
	tx2.commit();

	tx1.complete();

	tx2.complete();

	cout << "B: Tx1=" << tx1.getStatus() << " Tx2=" << tx2.getStatus() << " [O="
			<< o << ",\tV1=" << v1 << ",\tV2=" << v2 << "\t]" << endl;
}

void caseC() {
	long o = 100;

	Transaction tx1(1);
	Transaction tx2(2);

	tx1.write_l(&o, 200);
	tx1.commit();

	tx2.write_l(&o, 300);
	tx2.commit();

	tx1.complete();

	tx2.complete();

	cout << "C: Tx1=" << tx1.getStatus() << " Tx2=" << tx2.getStatus() << " [O="
			<< o << "\t]" << endl;
}

void caseD() {
	long o = 100;

	Transaction tx1(1);
	Transaction tx2(2);

	tx2.write_l(&o, 300);
	tx2.commit();

	tx1.write_l(&o, 200);
	tx1.commit();

	tx1.complete();

	tx2.complete();

	cout << "D: Tx1=" << tx1.getStatus() << " Tx2=" << tx2.getStatus() << " [O="
			<< o << "\t]" << endl;
}

void caseE() {
	long o = 100;

	Transaction tx1(1);
	Transaction tx2(2);

	tx1.write_l(&o, 200);
	tx1.commit();

	long v2 = tx2.read_l(&o);
	tx2.commit();

	tx1.complete();

	tx2.complete();

	cout << "E: Tx1=" << tx1.getStatus() << " Tx2=" << tx2.getStatus() << " [O="
			<< o << ",\tV2=" << v2 << "\t]" << endl;
}

void caseF() {
	long o = 100;

	Transaction tx1(1);
	Transaction tx2(2);

	long v2 = tx2.read_l(&o);
	tx2.commit();

	tx1.write_l(&o, 200);
	tx1.commit();

	tx1.complete();

	tx2.complete();

	cout << "F: Tx1=" << tx1.getStatus() << " Tx2=" << tx2.getStatus() << " [O="
			<< o << ",\tV2=" << v2 << "\t]" << endl;
}

void caseG() {
	long o = 100;

	Transaction tx1(1);
	Transaction tx2(2);

	long v1 = tx1.read_l(&o);
	tx1.commit();

	tx2.write_l(&o, 300);
	tx2.commit();

	tx1.complete();

	tx2.complete();

	cout << "G: Tx1=" << tx1.getStatus() << " Tx2=" << tx2.getStatus() << " [O="
			<< o << ",\tV1=" << v1 << "\t]" << endl;
}

void caseH() {
	long o = 100;

	Transaction tx1(1);
	Transaction tx2(2);

	tx2.write_l(&o, 300);
	tx2.commit();

	long v1 = tx1.read_l(&o);
	tx1.commit();

	tx1.complete();

	tx2.complete();

	cout << "H: Tx1=" << tx1.getStatus() << " Tx2=" << tx2.getStatus() << " [O="
			<< o << ",\tV1=" << v1 << "\t]" << endl;
}

int MAIN_OWB_CASES(int argc, char *argv[]) {
	cout << "!!!Hello OWB - word version!!!" << endl;
	testSeq(true, true);
	testSeq(true, false);
	testSeq(false, true);
	testSeq(false, false);
	caseA();
	caseB();
	caseC();
	caseD();
	caseE();
	caseF();
	caseG();
	caseH();
	cout << "!!!Bye OWB!!!" << endl;
	return 0;
}
