#include <stdio.h>
#include <pthread.h>
#include <unistd.h>
#include <stdlib.h>
#include <sched.h>
#include <linux/unistd.h>
#include <sys/syscall.h>
#include <errno.h>

#include "../conf.h"

#include "../algs/commons.hpp"

#define INC_TO 1000000 // one million...

int global_int = 0;

void *thread_routine(void *arg) {
	commons::stick_worker((int)(long)arg);
	for (int i = 0; i < INC_TO; i++) {
//		global_int++;
//		https://gcc.gnu.org/onlinedocs/gcc-4.4.3/gcc/Atomic-Builtins.html
		__sync_fetch_and_add(&global_int, 1);
	}
	return NULL;
}

int MAIN_ATOMIC(int argc, char *argv[]) {

//	float f = (float)1/3;
//	union { float f;  void* v;  } converter;
//	converter.f = f;
//	void* vv = converter.v;
//	union { float f;  void* v;  } converter2;
//	converter2.v = vv;
//	std::cout << converter2.f;
	int procs = commons::run_cpu_workers(thread_routine, commons::get_cpu_workers(), -1);
	printf("After doing all the math, global_int value is: %d\n", global_int);
	printf("Expected value is: %d\n", INC_TO * procs);
	return 0;
}
