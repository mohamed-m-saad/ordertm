
#include <iostream>
using namespace std;

#include "../conf.h"

#include "../algs/commons.hpp"

#include <pthread.h>
#include <sched.h>

volatile long sum = 0;

inline long long getElapsedTime()
{
    struct timespec t;
    clock_gettime(CLOCK_REALTIME, &t);

    return (((long long)t.tv_sec) * 1000000000L) + ((long long)t.tv_nsec);
}

void* func(void* arg){
	long long startTime = getElapsedTime();
	for(int i=0; i<100000; i++)
		for(int j=0; j<10000; j++)
			sum = i*sum;
	long long endTime = getElapsedTime();
	INFO("Hi " << pthread_self() << " " << (long)arg << "\t" << startTime << " " << endTime << " " << (endTime - startTime));
	return NULL;
}

void startup(int w, int sched){
	cout << "_______________________________________________________________" << endl;

	pthread_t *thrs = (pthread_t *) malloc(sizeof(pthread_t) * w); // Getting number of CPUs
	for(int i=0; i<w; i++){
		pthread_attr_t custom_sched_attr;

		pthread_attr_init(&custom_sched_attr);
		pthread_attr_setscope(&custom_sched_attr, PTHREAD_SCOPE_SYSTEM);
		pthread_attr_setinheritsched(&custom_sched_attr, PTHREAD_EXPLICIT_SCHED);

		if(sched >= 0){
			int max_prio, min_prio;
			struct sched_param s_param;
			pthread_attr_setschedpolicy(&custom_sched_attr, sched);
			min_prio = sched_get_priority_min(sched);
			max_prio = sched_get_priority_max(sched);
//			INFO("Sched= " << sched << " range " << min_prio << ":" << max_prio);
//			s_param.sched_priority = min_prio + w;
//			s_param.sched_priority = max_prio - w;
			s_param.sched_priority = max_prio;
			pthread_attr_setschedparam(&custom_sched_attr, &s_param);
		}

		if (pthread_create(&thrs[i], &custom_sched_attr, func, (void *) (long) i)) {
			cout << "fail " << i << endl;
			exit(1);
		}
	}

	for (int i = 0; i < w; i++){
		pthread_join(thrs[i], NULL);
	}
}

int MAIN_SCHED(int argc, char *argv[]) {
   int workers = 1;
   int sched = -100;
   int opt;    // parse the command-line options
   while ((opt = getopt(argc, argv, "w:fr")) != -1) {
		switch(opt) {
		  case 'w': workers = strtol(optarg, NULL, 10); break;
		  case 'f': sched = SCHED_FIFO; break;
		  case 'r': sched = SCHED_RR; break;
		}
	}

	startup(workers, sched);
	return 0;
}
