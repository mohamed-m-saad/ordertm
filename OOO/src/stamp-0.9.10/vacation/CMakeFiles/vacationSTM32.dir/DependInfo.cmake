# The set of languages for which implicit dependencies are needed:
SET(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
SET(CMAKE_DEPENDS_CHECK_CXX
  "/home/mohamed/Dropbox/workspace/rstm/stamp-0.9.10/lib/list.c" "/home/mohamed/Dropbox/workspace/rstm/stamp-0.9.10/vacation/CMakeFiles/vacationSTM32.dir/__/lib/list.c.o"
  "/home/mohamed/Dropbox/workspace/rstm/stamp-0.9.10/lib/mt19937ar.c" "/home/mohamed/Dropbox/workspace/rstm/stamp-0.9.10/vacation/CMakeFiles/vacationSTM32.dir/__/lib/mt19937ar.c.o"
  "/home/mohamed/Dropbox/workspace/rstm/stamp-0.9.10/lib/pair.c" "/home/mohamed/Dropbox/workspace/rstm/stamp-0.9.10/vacation/CMakeFiles/vacationSTM32.dir/__/lib/pair.c.o"
  "/home/mohamed/Dropbox/workspace/rstm/stamp-0.9.10/lib/random.c" "/home/mohamed/Dropbox/workspace/rstm/stamp-0.9.10/vacation/CMakeFiles/vacationSTM32.dir/__/lib/random.c.o"
  "/home/mohamed/Dropbox/workspace/rstm/stamp-0.9.10/lib/rbtree.c" "/home/mohamed/Dropbox/workspace/rstm/stamp-0.9.10/vacation/CMakeFiles/vacationSTM32.dir/__/lib/rbtree.c.o"
  "/home/mohamed/Dropbox/workspace/rstm/stamp-0.9.10/lib/thread.c" "/home/mohamed/Dropbox/workspace/rstm/stamp-0.9.10/vacation/CMakeFiles/vacationSTM32.dir/__/lib/thread.c.o"
  "/home/mohamed/Dropbox/workspace/rstm/stamp-0.9.10/vacation/client.c" "/home/mohamed/Dropbox/workspace/rstm/stamp-0.9.10/vacation/CMakeFiles/vacationSTM32.dir/client.c.o"
  "/home/mohamed/Dropbox/workspace/rstm/stamp-0.9.10/vacation/customer.c" "/home/mohamed/Dropbox/workspace/rstm/stamp-0.9.10/vacation/CMakeFiles/vacationSTM32.dir/customer.c.o"
  "/home/mohamed/Dropbox/workspace/rstm/stamp-0.9.10/vacation/manager.c" "/home/mohamed/Dropbox/workspace/rstm/stamp-0.9.10/vacation/CMakeFiles/vacationSTM32.dir/manager.c.o"
  "/home/mohamed/Dropbox/workspace/rstm/stamp-0.9.10/vacation/reservation.c" "/home/mohamed/Dropbox/workspace/rstm/stamp-0.9.10/vacation/CMakeFiles/vacationSTM32.dir/reservation.c.o"
  "/home/mohamed/Dropbox/workspace/rstm/stamp-0.9.10/vacation/vacation.c" "/home/mohamed/Dropbox/workspace/rstm/stamp-0.9.10/vacation/CMakeFiles/vacationSTM32.dir/vacation.c.o"
  )
SET(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
SET(CMAKE_TARGET_DEFINITIONS
  "HAVE_CONFIG_H"
  "LIST_NO_DUPLICATES"
  "MAP_USE_RBTREE"
  "STM"
  "STM_API_STAMP"
  )

# Targets to which this target links.
SET(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/mohamed/Dropbox/workspace/rstm/libstm/CMakeFiles/stm32.dir/DependInfo.cmake"
  )

# The include file search paths:
SET(CMAKE_C_TARGET_INCLUDE_PATH
  "."
  "include"
  "stamp-0.9.10/lib"
  "stamp-0.9.10"
  )
SET(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
