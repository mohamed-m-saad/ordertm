################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../src/algs/oul_kill.cpp \
../src/algs/oul_speculative.cpp \
../src/algs/oul_steal.cpp \
../src/algs/owb.cpp \
../src/algs/owbage.cpp \
../src/algs/owbrh.cpp \
../src/algs/stmlite.cpp \
../src/algs/tl2.cpp \
../src/algs/undolog_invis.cpp \
../src/algs/undolog_vis.cpp 

OBJS += \
./src/algs/oul_kill.o \
./src/algs/oul_speculative.o \
./src/algs/oul_steal.o \
./src/algs/owb.o \
./src/algs/owbage.o \
./src/algs/owbrh.o \
./src/algs/stmlite.o \
./src/algs/tl2.o \
./src/algs/undolog_invis.o \
./src/algs/undolog_vis.o 

CPP_DEPS += \
./src/algs/oul_kill.d \
./src/algs/oul_speculative.d \
./src/algs/oul_steal.d \
./src/algs/owb.d \
./src/algs/owbage.d \
./src/algs/owbrh.d \
./src/algs/stmlite.d \
./src/algs/tl2.d \
./src/algs/undolog_invis.d \
./src/algs/undolog_vis.d 


# Each subdirectory must supply rules for building sources it contributes
src/algs/%.o: ../src/algs/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: Cross G++ Compiler'
	g++ -DNDEBUG -O0 -g3 -w -c -fmessage-length=0 -std=c++11 $(CPPFLAGS) -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


