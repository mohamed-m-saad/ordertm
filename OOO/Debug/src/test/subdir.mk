################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../src/test/test_atomic.cpp \
../src/test/test_benchmark.cpp \
../src/test/test_hash.cpp \
../src/test/test_owb_cases.cpp \
../src/test/test_sched.cpp 

OBJS += \
./src/test/test_atomic.o \
./src/test/test_benchmark.o \
./src/test/test_hash.o \
./src/test/test_owb_cases.o \
./src/test/test_sched.o 

CPP_DEPS += \
./src/test/test_atomic.d \
./src/test/test_benchmark.d \
./src/test/test_hash.d \
./src/test/test_owb_cases.d \
./src/test/test_sched.d 


# Each subdirectory must supply rules for building sources it contributes
src/test/%.o: ../src/test/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: Cross G++ Compiler'
	g++ -DNDEBUG -O0 -g3 -w -c -fmessage-length=0 -std=c++11 $(CPPFLAGS) -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


