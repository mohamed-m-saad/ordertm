################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../src/stamp-0.9.10/genome/gene.c \
../src/stamp-0.9.10/genome/genome.c \
../src/stamp-0.9.10/genome/segments.c \
../src/stamp-0.9.10/genome/sequencer.c \
../src/stamp-0.9.10/genome/table.c 

OBJS += \
./src/stamp-0.9.10/genome/gene.o \
./src/stamp-0.9.10/genome/genome.o \
./src/stamp-0.9.10/genome/segments.o \
./src/stamp-0.9.10/genome/sequencer.o \
./src/stamp-0.9.10/genome/table.o 

C_DEPS += \
./src/stamp-0.9.10/genome/gene.d \
./src/stamp-0.9.10/genome/genome.d \
./src/stamp-0.9.10/genome/segments.d \
./src/stamp-0.9.10/genome/sequencer.d \
./src/stamp-0.9.10/genome/table.d 


# Each subdirectory must supply rules for building sources it contributes
src/stamp-0.9.10/genome/%.o: ../src/stamp-0.9.10/genome/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross GCC Compiler'
	g++ -DNDEBUG -O0 -g3 -w -c -fmessage-length=0  -std=c++11 $(CFLAGS) -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


