bench=$1
itr=10000
range=100000 #100000 #10
warm=$range
for u in 5 # 10 20
do
        echo "updates="$u"%"
        for w in 1 2 4 8
        do
                echo -n -e "   "$w" threads:\t"
                log=$bench"_"$u"_"$w
                rm -f $log
                for r in `seq 1 20`
                do
                        ./$bench $w $itr $u $range $warm >> $log
                done
                grep execution $log | awk 'BEGIN { min = 100000000; max = -1 } {total+=$4; if(max<$4) max=$4; if(min>$4) min=$4; count++} END {printf total/count "\t" max "\t" min }'
                echo -n -e "\t|\t"
                grep Total $log | awk -F',|=' 'BEGIN { min = 100000000; max=-1 } {total+=$12; if(max<$12) max=$12; if(min>$12) min=$12; count++} END {printf total/count "\t" max "\t" min }'
                echo -n -e "\t|\t"
                grep Ratio $log | awk -F',|=' 'BEGIN { min = 100000000; max=-1 } {total+=$12; if(max<$12) max=$12; if(min>$12) min=$12; count++} END {printf total/count "\t" max "\t" min }'
                echo -n -e "\t|\t"
                grep  Ratio -m 1  $log
        done
done
