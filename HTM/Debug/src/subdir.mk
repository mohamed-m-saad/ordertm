################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../src/array.cpp \
../src/bank.cpp \
../src/bench.cpp \
../src/disjoint.cpp \
../src/disjoint2.cpp \
../src/dummy.cpp \
../src/htmlgl.cpp \
../src/linkedlist.cpp \
../src/octonauts.cpp \
../src/redblacktree.cpp \
../src/thread.cpp \
../src/tpcc.cpp \
../src/trace.cpp 

C_SRCS += \
../src/list.c \
../src/oapi_burnticket.c \
../src/oapi_next2commit.c \
../src/oapi_timeline.c \
../src/oapi_timelines.c \
../src/rbtree.c 

OBJS += \
./src/array.o \
./src/bank.o \
./src/bench.o \
./src/disjoint.o \
./src/disjoint2.o \
./src/dummy.o \
./src/htmlgl.o \
./src/linkedlist.o \
./src/list.o \
./src/oapi_burnticket.o \
./src/oapi_next2commit.o \
./src/oapi_timeline.o \
./src/oapi_timelines.o \
./src/octonauts.o \
./src/rbtree.o \
./src/redblacktree.o \
./src/thread.o \
./src/tpcc.o \
./src/trace.o 

C_DEPS += \
./src/list.d \
./src/oapi_burnticket.d \
./src/oapi_next2commit.d \
./src/oapi_timeline.d \
./src/oapi_timelines.d \
./src/rbtree.d 

CPP_DEPS += \
./src/array.d \
./src/bank.d \
./src/bench.d \
./src/disjoint.d \
./src/disjoint2.d \
./src/dummy.d \
./src/htmlgl.d \
./src/linkedlist.d \
./src/octonauts.d \
./src/redblacktree.d \
./src/thread.d \
./src/tpcc.d \
./src/trace.d 


# Each subdirectory must supply rules for building sources it contributes
src/%.o: ../src/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: Cross G++ Compiler'
	g++ -O0 -g3 -Wall -c -fmessage-length=0 $(CPPFLAGS) -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

src/%.o: ../src/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross GCC Compiler'
	gcc -O0 -g3 -Wall -c -fmessage-length=0 $(CFLAGS) -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


