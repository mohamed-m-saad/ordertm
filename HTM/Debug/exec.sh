#emu="/home/users/msaad/sde-external-7.36.0-2015-12-10-lin/sde -hsw -rtm-mode full --" #-tsx-stats -- "
for w in 1 2 3 4 5 6 7 8 #12 16
do
     echo -n -e "   "$w" threads:\t"
     log="./log$2-t$w"
     rm -f $log
     for r in `seq 1 10`
     do
         $emu $@ -t$w >> $log
     done
     grep execution $log | awk 'BEGIN { min = 100000000; max = -1 } {total+=$4; if(max<$4) max=$4; if(min>$4) min=$4; count++} END {printf total/count "\t" max "\t" min }'
     echo -n -e "\t|\t"
     grep Total $log | awk -F',|=' 'BEGIN { min = 100000000; max=-1 } {total+=$12; if(max<$12) max=$12; if(min>$12) min=$12; count++} END {printf total/count "\t" max "\t" min }'
     echo -n -e "\t|\t"
     grep Ratio $log | awk -F',|=' 'BEGIN { min = 100000000; max=-1 } {total+=$12; if(max<$12) max=$12; if(min>$12) min=$12; count++} END {printf total/count "\t" max "\t" min }'
     echo -n -e "\t|\t"
     grep  Ratio -m 1  $log
done
