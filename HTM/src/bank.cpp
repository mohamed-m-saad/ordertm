//============================================================================
// Name        : HTM.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <stdlib.h>
#include <string.h>
#include <iostream>
#include <stdio.h>
#include <pthread.h>
#include <sys/time.h>
using namespace std;

#include "bench.hpp"
#include "api_htmgl.h"
#include "thread.h"
#include "stats.hpp"
#include "trace.hpp"

USE_TM

namespace bank{

struct trace* trace;
struct trace** traces;

long *account;

#define BALANCE	10000

void init() {
	trace = build_trace(1983, CFG.iterations);
	traces = split_trace(trace, CFG.threads);
	account = new long[CFG.range];
	printf("Bank of %d accounts\n", CFG.range);
	for(int i=0; i<CFG.range; i++)
		account[i] = BALANCE;
}

void run(void* p) {
	int processing = CFG.oprations;
	TM_THREAD_ENTER()
	struct trace* local_trace = traces[thread_getId()];
	STATS_START
	for (int i = 0; i < local_trace->length; i++) {
		struct trace_element e = local_trace->elements[i];
		int account1 = e.arg1 % CFG.range;
		int account2 = e.arg2 % CFG.range;
		long amount = e.arg3 % BALANCE;
		TM_BEGIN(e.age)
			for(int p=0; p<processing; p++)
				if(account[account1] > amount){
					account[account1] -= amount;
					account[account2] += amount;
				}
		TM_END()
	}
	STATS_END
	TM_THREAD_EXIT()
}

void destroy() {
}

}
