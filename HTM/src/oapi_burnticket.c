
#include "oapi_burnticket.h"

#define DEFAULT_TICKETS	9
#define DEFAULT_BACKOFF	1

struct ticket **tickets;
unsigned tickets_count = DEFAULT_TICKETS;
unsigned tickets_backoff = DEFAULT_BACKOFF;
unsigned tickets_threads_count;
unsigned volatile tickets_last_committed = 0;
