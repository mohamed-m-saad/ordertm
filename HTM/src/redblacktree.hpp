
#ifndef REDBLACKTREE_HPP_
#define REDBLACKTREE_HPP_

namespace redblacktree{
	void init();
	void destroy();
	void run(void* tid);
}

#endif
