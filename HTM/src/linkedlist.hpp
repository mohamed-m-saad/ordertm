
#ifndef LINKEDLIST_HPP_
#define LINKEDLIST_HPP_

namespace linkedlist{
	void init();
	void destroy();
	void run(void* tid);
}

#endif
