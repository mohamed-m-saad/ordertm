#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>

#include "bench.hpp"
#include "list.h"
#include "thread.h"
#include "stats.hpp"
#include "trace.hpp"

typedef list_t intset_t;

USE_TM

namespace linkedlist{

intset_t *set;
struct trace* trace;
struct trace** traces;

void init() {
	trace = build_trace(1983, CFG.iterations);
	traces = split_trace(trace, CFG.threads);
	set = list_alloc(NULL);
	printf("Linked List of %d range, %d initial entries, and %d update\n", CFG.range, CFG.warm, CFG.update);
	for (int i = 0; i < CFG.warm; i++) {
		list_insert(set, (void*) (i%CFG.range));
	}
}

void destroy() {
	list_print(set);
	list_free(set);
}

void run(void* p) {
	int processing = CFG.oprations;
	int update = CFG.update;
	int range = CFG.range;
	TM_THREAD_ENTER()
	struct trace* local_trace = traces[(unsigned)thread_getId()];
	STATS_START
	for (int i = 0; i < local_trace->length; i++) {
		struct trace_element e = local_trace->elements[i];
		int op = e.arg1 % 100;
		int val = e.arg2 % range;
		if (op < update) {
			if (op < update / 2) {
				/* Add random value */
				TM_BEGIN(e.age);
					for(int p=0; p<processing; p++)
						TMlist_insert(TM_ARG set, (void*) val);
				TM_END();
			} else {
				/* Remove random value */
				TM_BEGIN(e.age);
					for(int p=0; p<processing; p++)
						TMlist_remove(TM_ARG set, (void*) val);
				TM_END();
			}
		} else {
			/* Look for random value */
			TM_BEGIN(e.age);
				for(int p=0; p<processing; p++)
					TMlist_find(TM_ARG set, (void*) val);
			TM_END();
		}
	}
	STATS_END
	TM_THREAD_EXIT()
}

}
