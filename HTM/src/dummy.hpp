
#ifndef DUMMY_HPP_
#define DUMMY_HPP_

namespace dummy{
	void init();
	void destroy();
	void run(void* tid);
}

#endif
