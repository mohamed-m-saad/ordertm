/*
 * tpcc.h
 *
 *  Created on: Jan 2, 2016
 *      Author: root
 */

#ifndef TPCC_H_
#define TPCC_H_

#include <stdio.h>
#include <pthread.h>
#include <time.h>
#include <stdint.h>
#include <unistd.h>
#include <setjmp.h>
#include <unistd.h>
#include <sched.h>

#include "rand_r_32.h"

#define  NUM_ITEMS  50
//1000; // Correct overall # of items: 100,000
#define  NUM_WAREHOUSES  20
#define  NUM_DISTRICTS  50
//10000; //4;
#define  NUM_CUSTOMERS_PER_D  50
//300; //30;
#define  NUM_ORDERS_PER_D  50

#define  NUM_LINES_PER_O  5
//3000; //30;
#define  MAX_CUSTOMER_NAMES  50

#define  NUM_HISTORY_PER_C 10
//1000; // 10;

#define  DEFAULT_LENGTH  6

uint32_t gseed = 459;

typedef struct customer {
	int C_FIRST;
	int C_MIDDLE;
	int C_LAST;
	int C_STREET_1;
	int C_STREET_2;
	int C_CITY;
	int C_STATE;
	int C_ZIP;
	int C_PHONE;
	int C_SINCE;
	int C_CREDIT;
	double C_CREDIT_LIM;
	double C_DISCOUNT;
	double C_BALANCE;
	double C_YTD_PAYMENT;
	int C_PAYMENT_CNT;
	int C_DELIVERY_CNT;
	int C_DATA;
	customer() :
			C_CREDIT_LIM(50000.0), C_DISCOUNT(10.0), C_YTD_PAYMENT(10.0), C_PAYMENT_CNT(
					1), C_DELIVERY_CNT(0) {
		int r = rand_r_32(&gseed) % 100;
		C_CREDIT = r < 90 ? 1 : 2;
		C_DISCOUNT = (double) ((rand_r_32(&gseed) % 5000) * 0.0001);
	}
} customer_t;

typedef struct district {
	int D_NAME;
	int D_STREET_1;
	int D_STREET_2;
	int D_CITY;
	int D_STATE;
	int D_ZIP;

	double D_TAX;
	double D_YTD;
	int D_NEXT_O_ID;

	district() :
			D_YTD(30000.0), D_NEXT_O_ID(3001) {
		D_TAX = ((rand_r_32(&gseed) % 2000) * 0.0001);
	}
} district_t;

typedef struct history {
	int H_C_ID;
	int H_C_D_ID;
	int H_C_W_ID;
	int H_D_ID;
	int H_W_ID;
	int H_DATE;
	double H_AMOUNT;
	int H_DATA;
	history(int c_id, int d_id) :
			H_AMOUNT(10) {
		H_W_ID = rand_r_32(&gseed) % 100;
		H_D_ID = d_id;
		H_C_ID = c_id;
	}
	history() :
			H_AMOUNT(10) {
		H_W_ID = rand_r_32(&gseed) % 100;
		H_D_ID = rand_r_32(&gseed) % NUM_DISTRICTS;
		H_C_ID = rand_r_32(&gseed) % NUM_CUSTOMERS_PER_D;
	}
} history_t;

typedef struct item {
	int I_IM_ID;
	int I_NAME;
	float I_PRICE;
	int I_DATA;
	item() {
		I_PRICE = (float) (rand_r_32(&gseed) % 10000);
	}
} item_t;

typedef struct order {
	int O_C_ID;
	int O_ENTRY_D;
	int O_CARRIER_ID;
	int O_OL_CNT;
	bool O_ALL_LOCAL;
	order() :
			O_ALL_LOCAL(true) {
		O_C_ID = rand_r_32(&gseed) % 100;
		O_OL_CNT = 5 + rand_r_32(&gseed) % 11;
	}
} order_t;

typedef struct order_line {
	int OL_I_ID;
	int OL_SUPPLY_W_ID;
	int OL_DELIVERY_D;
	int OL_QUANTITY;
	int OL_AMOUNT;
	int OL_DIST_INFO;
	order_line() :
			OL_QUANTITY(5) {
		OL_I_ID = 1 + rand_r_32(&gseed) % 100000;
		OL_SUPPLY_W_ID = rand_r_32(&gseed) % 1000;
		int a = rand_r_32(&gseed) % 3000;
		if (a < 2101)
			OL_AMOUNT = 0;
		else {
			OL_AMOUNT = (1 + rand_r_32(&gseed) % (999999));
		}
	}
} order_line_t;

typedef struct stock {
	int S_QUANTITY;
	int S_DIST_01;
	int S_DIST_02;
	int S_DIST_03;
	int S_DIST_04;
	int S_DIST_05;
	int S_DIST_06;
	int S_DIST_07;
	int S_DIST_08;
	int S_DIST_09;
	int S_DIST_10;
	int S_YTD;
	int S_ORDER_CNT;
	int S_REMOTE_CNT;
	int S_DATA;

	stock() :
			S_YTD(0), S_ORDER_CNT(0), S_REMOTE_CNT(0) {
		S_QUANTITY = 10 + rand_r_32(&gseed) % (91);
		if (rand_r_32(&gseed) % (100) < 10) {
			S_DATA = 1000 + rand_r_32(&gseed) % 100; //orginal
		} else {
			S_DATA = rand_r_32(&gseed) % (100);
		}
	}
} stock_t;

typedef struct warehouse {
	int W_NAME;
	int W_STREET_1;
	int W_STREET_2;
	int W_CITY;
	int W_STATE;
	int W_ZIP;
	float W_TAX;
	float W_YTD;
	warehouse() :
			W_YTD(300000.0) {
		W_TAX = (rand_r_32(&gseed) % (2000) * 0.0001);
	}
} warehouse_t;

item_t items[NUM_ITEMS];

warehouse_t warehouses[NUM_WAREHOUSES];

stock_t stocks[NUM_WAREHOUSES][NUM_ITEMS];

district_t districts[NUM_WAREHOUSES][NUM_DISTRICTS];

customer_t customers[NUM_WAREHOUSES][NUM_DISTRICTS][NUM_CUSTOMERS_PER_D];

history_t histories[NUM_WAREHOUSES][NUM_DISTRICTS][NUM_CUSTOMERS_PER_D][NUM_HISTORY_PER_C];

order_t orders[NUM_WAREHOUSES][NUM_ORDERS_PER_D];

order_line_t order_lines[NUM_WAREHOUSES][NUM_ORDERS_PER_D][NUM_LINES_PER_O];



#endif /* TPCC_H_ */
