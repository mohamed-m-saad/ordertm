
#ifndef TPCC_HPP_
#define TPCC_HPP_

namespace tpcc{
	void init();
	void destroy();
	void run(void* tid);
}

#endif
