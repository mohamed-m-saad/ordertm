
#include "oapi_timelines.h"

#define TIMELINES_DEFAULT_FLAGS	32
#define TIMELINES_DEFAULT_BACKOFF	10

struct timelines_flag *timelines;
unsigned timelines_flags_count = TIMELINES_DEFAULT_FLAGS;
unsigned timelines_flags_index_mask;
volatile unsigned timelines_next_to_commit = 0;
unsigned timelines_backoff = TIMELINES_DEFAULT_BACKOFF;
unsigned timelines_tx_start[TIMELINES_MAX_THREADS];
