
#ifndef BANK_HPP_
#define BANK_HPP_

namespace bank{
	void init();
	void destroy();
	void run(void* tid);
}

#endif
