
/* =============================================================================
 *
 * Copyright (C) Stanford University, 2010.  All Rights Reserved.
 * Author: Sungpack Hong and Jared Casper
 *
 * =============================================================================
 */
#include "rtm.h"
#include <stdlib.h>
#include <sys/time.h>
#include <stdint.h>

#ifndef _STAMP_OAPI_NEXT2COMMIT_
#define _STAMP_OAPI_NEXT2COMMIT_

#define FUTURE_SHIFT					2

#define CYCLES_PER_FLAG_SHIFT			16
#define CACHE_LINE_SIZE                 64


static inline double get_current_time(){
	struct timeval start;
	gettimeofday(&start, NULL);
	return (start.tv_sec) * 1000 + (start.tv_usec) * 0.001;
}

static inline uint64_t get_cycles()
{
	/*
	 rdtsc:
		Loads the current value of the processor's time-stamp counter into the EDX:EAX registers. The time-stamp counter
		is contained in a 64-bit MSR. The high-order 32 bits of the MSR are loaded into the EDX register, and the low-order
		 32 bits are loaded into the EAX register. The processor monotonically increments the time-stamp counter MSR every
		 clock cycle and resets it to 0 whenever the processor is reset. See "Time Stamp Counter" in Chapter 15 of the
		 IA-32 Intel Architecture Software Developer's Manual, Volume 3 for specific details of the time stamp counter behavior.
		When in protected or virtual 8086 mode, the time stamp disable (TSD) flag in register CR4 restricts the use of the
		RDTSC instruction as follows. When the TSD flag is clear, the RDTSC instruction can be executed at any privilege level;
		when the flag is set, the instruction can only be executed at privilege level 0. (When in real-address mode, the RDTSC
		instruction is always enabled.) The time-stamp counter can also be read with the RDMSR instruction, when executing at
		privilege level 0.
		The RDTSC instruction is not a serializing instruction. Thus, it does not necessarily wait until all previous instructions
		have been executed before reading the counter. Similarly, subsequent instructions may begin execution before the read
		operation is performed.
		This instruction was introduced into the IA-32 Architecture in the Pentium processor.
	 */
	/*
	 rdtscp:
		Loads the current value of the processor’s time-stamp counter (a 64-bit MSR) into the EDX:EAX registers and also loads
		the IA32_TSC_AUX MSR (address C000_0103H) into the ECX register. The EDX register is loaded with the high-order 32 bits
		of the IA32_TSC MSR; the EAX register is loaded with the low-order 32 bits of the IA32_TSC MSR; and the ECX register is
		loaded with the low-order 32-bits of IA32_TSC_AUX MSR. On processors that support the Intel 64 architecture, the high-order
		32 bits of each of RAX, RDX, and RCX are cleared.
		The processor monotonically increments the time-stamp counter MSR every clock cycle and resets it to 0 whenever the processor
		is reset. See “Time Stamp Counter” in Chapter 17 of the Intel® 64 and IA-32 Architectures Software Developer’s Manual, Volume
		3B, for specific details of the time stamp counter behavior.
		When in protected or virtual 8086 mode, the time stamp disable (TSD) flag in register CR4 restricts the use of the RDTSCP
		instruction as follows. When the TSD flag is clear, the RDTSCP instruction can be executed at any privilege level; when the
		flag is set, the instruction can only be executed at privilege level 0. (When in real-address mode, the RDTSCP instruction
		is always enabled.)
		The RDTSCP instruction waits until all previous instructions have been executed before reading the counter. However, subsequent
		instructions may begin execution before the read operation is performed.
		The presence of the RDTSCP instruction is indicated by CPUID leaf 80000001H, EDX bit 27. If the bit is set to 1 then RDTSCP
		is present on the processor.
	 */
  uint64_t t;
  __asm volatile ("rdtscp" : "=A"(t));
  return t;
}

struct timeline_flag{
	unsigned char padding[2*CACHE_LINE_SIZE-4];
	volatile unsigned val;
};

#define MEMALIGN(ptr, alignment, size)    posix_memalign((void **)(ptr),  (alignment), (size))
#define r_align(n, r)                     (((n) + (r) - 1) & -(r))
#define cache_align(n)                    r_align(n , CACHE_LINE_SIZE)
#define pad_to_cache_line(n)              (cache_align(n) - (n))

static void* _liblock_allocate(int n){
    void* res = 0;
    if((MEMALIGN(&res, CACHE_LINE_SIZE, cache_align(n)) < 0) || !res){
        return 0; //printf("MEMALIGN(%llu, %llu)", (unsigned long long)n, (unsigned long long)cache_align(n));
    }
    return res;
}

#define TM_STARTUP(X)			GL_INIT(the_lock);							\
								timeline = (struct timeline_flag*)_liblock_allocate(sizeof(struct timeline_flag) * timeline_flags_count);	\
								timeline_flags_index_mask = timeline_flags_count - 1;	\
								for(unsigned j=0; j<timeline_flags_count; j++)			\
									timeline[j].val = 0;								\

#define TM_SHUTDOWN()

#define USE_TM 					extern GL_T the_lock;								\
								extern struct timeline_flag *timeline;				\
								extern volatile unsigned timeline_next_to_commit;	\
								extern unsigned timeline_backoff;					\
								extern unsigned timeline_flags_count;				\
								extern unsigned timeline_flags_index_mask;			\
								extern unsigned timeline_window;

#define TM_THREAD_ENTER()
#define TM_THREAD_EXIT()

# define TM_BEGIN(X)			{ 																\
									int tries = 5;  											\
									unsigned age = X;											\
									bool commit_directly = false;								\
									while (1) { 												\
										while (IS_GL_LOCKED(the_lock)) { __asm__ ( "pause;" ); } 	\
										commit_directly = timeline_next_to_commit == age;		\
										int status = _xbegin(); 								\
										if (status == _XBEGIN_STARTED) {if (IS_GL_LOCKED(the_lock)) _xabort(25); break; } \
										if ((status & _XABORT_EXPLICIT) && _XABORT_CODE(status)==27) {					  \
											ordering_abort++;									\
										} else if ((status & _XABORT_EXPLICIT) && _XABORT_CODE(status)==25) {			  \
											gl_abort++;											\
										} else if (status & _XABORT_CAPACITY){					\
											capacity_abort++;									\
										} else if (status & _XABORT_CONFLICT){					\
											conflict_abort++;									\
										} else{													\
											other_abort++;}										\
										tries--;												\
										commit_directly = timeline_next_to_commit == age;		\
										if (tries <= 0) {   									\
											while(timeline_next_to_commit != age); 				\
											GL_LOCK(the_lock); 						\
											break;  											\
										}   													\
									}

# define TM_END()					/* calculate the pivot */									\
									int last_seen_pivot = -1; int max_pivots = 0;				\
									while(true){												\
										long c = get_cycles();									\
										int pivot = (c>>CYCLES_PER_FLAG_SHIFT) & timeline_flags_index_mask;		\
										if(pivot != last_seen_pivot){							\
											last_seen_pivot = pivot; max_pivots++;				\
											if(max_pivots > 10) _xabort(27);					\
										}														\
										if(commit_directly || timeline[pivot].val == age){		\
											/* perform commit */								\
											if (tries > 0) {									\
												_xend();										\
												htm_count++;									\
											} else {											\
												GL_UNLOCK(the_lock);				\
												gl_count++;										\
											}													\
											/*printf("%d %d %f %lu %lu %d/%d \n", thread_getId(), age, get_current_time(), c, (c>>CYCLES_PER_FLAG_SHIFT), pivot, timeline_flags_count);*/	\
											/* assure the previous thread finished flagging */	\
											while(timeline_next_to_commit !=  age);                                 \
											/* set future flags */								\
											int start = pivot+FUTURE_SHIFT;						\
											int last = pivot+timeline_window+FUTURE_SHIFT;		\
											for(int w=start; w<last; w++)						\
												timeline[w & timeline_flags_index_mask].val = age + 1;		\
											timeline_next_to_commit =  age + 1;					\
											break;												\
										}														\
										for(unsigned i=0; i<timeline_backoff; i++)				\
											__asm__ ( "nop;" );	/* wait */						\
									}															\
								}

# define TM_ABORT(X)			_xabort(X);

#endif
