//============================================================================
// Name        : HTM.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <stdlib.h>
#include <string.h>
#include <iostream>
#include <stdio.h>
#include <pthread.h>
#include <sys/time.h>
using namespace std;

#include "bench.hpp"
#include "api_htmgl.h"
#include "thread.h"
#include "stats.hpp"
#include "trace.hpp"

USE_TM

namespace disjoint{

#define CACHE_PAGE	8192
#define CACHE_LINE_SIZE                   64

#define MEMALIGN(ptr, alignment, size)    posix_memalign((void **)(ptr),  (alignment), (size))
#define r_align(n, r)                     (((n) + (r) - 1) & -(r))
#define cache_align(n)                    r_align(n , CACHE_LINE_SIZE)
#define pad_to_cache_line(n)              (cache_align(n) - (n))

inline void* liblock_allocate(size_t n)
{
    void* res = 0;
    if((MEMALIGN(&res, CACHE_LINE_SIZE, cache_align(n)) < 0) || !res){
        printf("MEMALIGN(%llu, %llu)", (unsigned long long)n, (unsigned long long)cache_align(n));
    }
    return res;
}

struct cache_line{
	unsigned char val;
	unsigned char dummy[CACHE_PAGE-1];
};

struct cache_line **data;

void init() {
	printf("Disjoint of %d entries [%d per thread, entry size %d]\n", CFG.range*CFG.threads, CFG.range, sizeof(struct cache_line));
	data = (struct cache_line**)liblock_allocate(sizeof(struct cache_line*) * CFG.range*CFG.threads);
	for(int i=0; i<CFG.range*CFG.threads; i++){
		data[i] = (struct cache_line*)liblock_allocate(sizeof(struct cache_line));
	}
}

void run(void* p) {
	int processing = CFG.oprations;
	unsigned range = CFG.range*CFG.threads;
	unsigned loop = CFG.iterations/CFG.threads;
	unsigned threads = CFG.threads;
	TM_THREAD_ENTER()
	STATS_START
	unsigned first_entry = thread_getId();
	unsigned _age = thread_getId();
	for(unsigned i=0; i<loop; i++){
		TM_BEGIN(_age)
			for(int p=0; p<processing; p++)
				for(unsigned j=first_entry; j<range; j+=threads)
						data[j]->val = (unsigned char)j;
		TM_END()
		_age += threads;
	}
	STATS_END
	TM_THREAD_EXIT()
}

void destroy() {
}

}
