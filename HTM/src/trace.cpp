/*
 * trace.cpp
 *
 *  Created on: Dec 8, 2015
 *      Author: Mohamed M. Saad
 */
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>

#include "trace.hpp"

struct trace* build_trace(int seed, int length){
	unsigned local_seed = seed;
	unsigned *s = &local_seed;
	struct trace* t = new struct trace(length);
	for(int i=0; i<length; i++){
		t->elements[i].age = i;
		t->elements[i].arg1 = rand_r(s);
		t->elements[i].arg2 = rand_r(s);
		t->elements[i].arg3 = rand_r(s);
		t->elements[i].arg4 = rand_r(s);
		t->elements[i].arg5 = rand_r(s);
	}
	return t;
}

struct trace** split_trace(struct trace* origin, int nodes){
	struct trace** sub_traces = new struct trace*[nodes];
	for(int i=0; i<nodes; i++){
		int min_length = origin->length / nodes;
		int length = min_length;
		if(i==nodes-1){ // last sub-trace
			length = origin->length - length * (nodes-1);
		}
		sub_traces[i] = new struct trace(length);
		for(int j=0, k=i; j<min_length; j++, k+=nodes)
			sub_traces[i]->elements[j] = origin->elements[k];
		if(i==nodes-1){ // last sub-trace
			int k = min_length * nodes;
			for(int j=min_length; j<length; j++, k++)
				sub_traces[i]->elements[j] = origin->elements[k];
		}
	}
	return sub_traces;
}

