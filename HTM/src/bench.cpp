//============================================================================
// Name        : HTM.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <ctype.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <iostream>
#include <stdio.h>
#include <pthread.h>
#include <sys/time.h>

#include "rtm.h"
#include "thread.h"
#include "stats.hpp"
#include "api_htmgl.h"

#include "bank.hpp"
#include "array.hpp"
#include "linkedlist.hpp"
#include "redblacktree.hpp"
#include "disjoint.hpp"
#include "disjoint2.hpp"
#include "tpcc.hpp"
#include "dummy.hpp"

#include "bench.hpp"
#include "trace.hpp"

USE_TM

struct config CFG;

void (*init_func)();
void (*run_func)(void* p);
void (*destroy_func)();

#define BENCH(X)					if(!strcmp(optarg,#X)){			\
										init_func = X::init;		\
										run_func = X::run;			\
										destroy_func = X::destroy;	\
									}
int main(int argc, char** argv) {

	CFG.threads = 1;
	CFG.iterations = 1;
	CFG.oprations = 1;
	CFG.update = 0;
	CFG.range = 0;
	CFG.warm = 0;

    int opt;
	while ((opt = getopt(argc, argv, "b:t:n:o:u:r:w:")) != -1) {
		switch (opt) {
			case 'b':
				printf("!!!Hello HTM [%s]!!\n", optarg);
				BENCH(bank)
				BENCH(array)
				BENCH(linkedlist)
				BENCH(redblacktree)
				BENCH(disjoint)
				BENCH(disjoint2)
				BENCH(tpcc)
				BENCH(dummy)
				break;
			case 't': CFG.threads = atoi(optarg); break;
			case 'n': CFG.iterations = atoi(optarg); break;
			case 'o': CFG.oprations = atoi(optarg); break;
			case 'u': CFG.update = atoi(optarg); break;
			case 'r': CFG.range = atoi(optarg); break;
			case 'w': CFG.warm = atoi(optarg); break;
			default:
				printf("Usage: %s [b:bench] [t:threads] [n:transactions] [o:operations] [u:update] [r:range] [w:warm]", argv[0]);
				printf("\nExamples: ");
				printf("\n\t %s -bbank  -t2 -n100000 -o10 -r1000 ", argv[0]);
				printf("\n\t %s -barray -t2 -n100000 -o10 -r1000 ", argv[0]);
				printf("\n\t %s -blinkedlist   -t2 -n1000 -o10 -r1000 -u20 -w500", argv[0]);
				printf("\n\t %s -bredblacktree -t2 -n1000 -o10 -r1000 -u20 -w500", argv[0]);
				printf("\n\t %s -bdisjoint  -t2 -n10000 -o10 -r100 ", argv[0]);
				printf("\n\t %s -bdisjoint2 -t2 -n10000 -o10 -r30 -w500", argv[0]);
				printf("\n\t %s -btpcc  -t2 -n10000 -o10", argv[0]);
				printf("\n\t %s -bdummy -t2 -n10000 -o10", argv[0]);
				printf("\n");
//				exit(EXIT_FAILURE);
		}
	}
	while(CFG.iterations%CFG.threads != 0) CFG.iterations++;
	for(int i=0; i<argc; i++)
		printf("%s ", argv[i]);
	printf("\n");
	printf("[t:threads=%d] [n:transactions=%d] [o:operations=%d] [u:update=%d] [r:range=%d] [w:warm=%d]\n",
			CFG.threads, CFG.iterations, CFG.oprations, CFG.update, CFG.range, CFG.warm);
	init_func();
	TM_STARTUP(CFG.threads)

	thread_startup (CFG.threads);
	struct timeval start, stop;
	gettimeofday(&start, NULL);
	thread_start(run_func, NULL);
	gettimeofday(&stop, NULL);
	destroy_func();
	thread_printStatistics();
	thread_shutdown();
	printf("execution time = %lf (ms)\n", (stop.tv_sec - start.tv_sec) * 1000 + (stop.tv_usec - start.tv_usec) * 0.001);

	TM_SHUTDOWN()
	printf("!!!Bye!!!\n");
	return 0;
}
