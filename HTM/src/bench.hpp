/*
 * linkedlist.hpp
 *
 *  Created on: Dec 5, 2015
 *      Author: root
 */

#ifndef BENCH_HPP_
#define BENCH_HPP_

struct config{
	int threads;
	int iterations;
	int oprations;
	int update;
	int range;
	int warm;
};

extern struct config CFG;

namespace NAMESPACE{
	void init();
	void destroy();
	void run(void* tid);
}

#endif /* BENCH_HPP_ */
