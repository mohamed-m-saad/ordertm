
/* =============================================================================
 *
 * Copyright (C) Stanford University, 2010.  All Rights Reserved.
 * Author: Sungpack Hong and Jared Casper
 *
 * =============================================================================
 */
#include "rtm.h"
#include "gl.h"
//#include "oapi_next2commit.h"
//#include "oapi_noorder.h"
//#include "oapi_burnticket.h"
#include "oapi_timeline.h"
//#include "oapi_timelines.h"

#ifndef _STAMP_API_FOR_HTMGL_
#define _STAMP_API_FOR_HTMGL_

#define TM_ARG
#define TM_ARGDECL
#define TM_ARG_ALONE

#define STM_READ(var)      	(*var)
#define STM_WRITE(var,val) 	({*var = val; var;})

#define TM_SHARED_READ(var)           (var)
#define TM_SHARED_READ_P(var)         (var)
#define TM_SHARED_READ_F(var)         (var)

#define TM_SHARED_WRITE(var, val)     ({var = val; var;})
#define TM_SHARED_WRITE_P(var, val)   ({var = val; var;})
#define TM_SHARED_WRITE_F(var, val)   ({var = val; var;})

#define TM_LOCAL_WRITE(var, val)      ({var = val; var;})
#define TM_LOCAL_WRITE_P(var, val)    ({var = val; var;})
#define TM_LOCAL_WRITE_F(var, val)    ({var = val; var;})

#define P_MALLOC(a) malloc(a)
#define TM_MALLOC(a) malloc(a)

#define P_FREE(a)
#define TM_FREE(a)

#endif
