/*
 * gl.h
 *
 *  Created on: Jan 11, 2016
 *      Author: root
 */

#ifndef GL_H_
#define GL_H_

#define IS_GL_LOCKED(lock)      *((volatile int*)(lock)) != 0
#define GL_T                    pthread_mutex_t*
#define GL_INIT(lock)           lock = new pthread_mutex_t; pthread_mutex_init(lock, NULL)
#define GL_LOCK(lock)           pthread_mutex_lock(lock)
#define GL_UNLOCK(lock)         pthread_mutex_unlock(lock)
#define INIT_LOCK               GL_T the_lock;

/*
#define IS_GL_LOCKED(lock)      *((volatile int*)(&lock)) != 0
#define GL_T                    pthread_mutex_t
#define GL_INIT(lock)           pthread_mutex_init(&(lock), NULL)
#define GL_LOCK(lock)           pthread_mutex_lock(&(lock))
#define GL_UNLOCK(lock)         pthread_mutex_unlock(&(lock))
#define INIT_LOCK 				GL_T the_lock;
*/

/*
#define IS_GL_LOCKED(lock)      *lock != 0
#define GL_T                    volatile int*
#define GL_INIT(lock)           lock = new volatile int; *lock = 0;
#define GL_LOCK(lock)           *lock = 1;
#define GL_UNLOCK(lock)         *lock = 0;
#define INIT_LOCK               GL_T the_lock;
*/

#endif /* GL_H_ */
