
#include "oapi_timeline.h"

#define DEFAULT_FLAGS	32
#define DEFAULT_WINDOW  10
#define DEFAULT_BACKOFF	10

struct timeline_flag *timeline;
unsigned timeline_flags_count = DEFAULT_FLAGS;
unsigned timeline_flags_index_mask;
volatile unsigned timeline_next_to_commit = 0;
unsigned timeline_window = DEFAULT_WINDOW;
unsigned timeline_backoff = DEFAULT_BACKOFF;
