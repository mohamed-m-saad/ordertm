
/* =============================================================================
 *
 * Copyright (C) Stanford University, 2010.  All Rights Reserved.
 * Author: Sungpack Hong and Jared Casper
 *
 * =============================================================================
 */
#include "rtm.h"

#ifndef _STAMP_OAPI_NEXT2COMMIT_
#define _STAMP_OAPI_NEXT2COMMIT_

#define TM_STARTUP(X)			GL_INIT(the_lock);
#define TM_SHUTDOWN()
#define USE_TM 					extern GL_T the_lock;

#define TM_THREAD_ENTER()
#define TM_THREAD_EXIT()

# define TM_BEGIN(X)			{ \
									int tries = 5;  \
									while (1) { \
										while (IS_GL_LOCKED(the_lock)) { __asm__ ( "pause;" ); } \
										int status = _xbegin(); \
										if (status == _XBEGIN_STARTED) {if (IS_GL_LOCKED(the_lock)) _xabort(25); break; } \
										if ((status & _XABORT_EXPLICIT) && _XABORT_CODE(status)==25) {					  \
											gl_abort++;												\
										} else if (status & _XABORT_CAPACITY){										\
											capacity_abort++;												\
										} else if (status & _XABORT_CONFLICT){										\
											conflict_abort++;												\
										} else{																		\
											other_abort++;}\
										tries--;	\
										if (tries <= 0) {   \
											GL_LOCK(the_lock); \
											break;  \
										}   \
									}

# define TM_END()					if (tries > 0) {    \
										_xend();    \
										htm_count++;\
									} else {    \
										GL_UNLOCK(the_lock);    \
										gl_count++;\
									}\
								};

# define TM_ABORT(X)			_xabort(X);

#endif
