
/* =============================================================================
 *
 * Copyright (C) Stanford University, 2010.  All Rights Reserved.
 * Author: Sungpack Hong and Jared Casper
 *
 * =============================================================================
 */
#include "rtm.h"

#ifndef _STAMP_OAPI_NEXT2COMMIT_
#define _STAMP_OAPI_NEXT2COMMIT_

#define CACHE_LINE_SIZE                   64

struct ticket{
	unsigned char padding[2*CACHE_LINE_SIZE-1];
	volatile unsigned char val;
};

#define MEMALIGN(ptr, alignment, size)    posix_memalign((void **)(ptr),  (alignment), (size))
#define r_align(n, r)                     (((n) + (r) - 1) & -(r))
#define cache_align(n)                    r_align(n , CACHE_LINE_SIZE)
#define pad_to_cache_line(n)              (cache_align(n) - (n))

static void* _liblock_allocate(int n){
    void* res = 0;
    if((MEMALIGN(&res, CACHE_LINE_SIZE, cache_align(n)) < 0) || !res){
        return 0; //printf("MEMALIGN(%llu, %llu)", (unsigned long long)n, (unsigned long long)cache_align(n));
    }
    return res;
}

#define TM_STARTUP(X)			GL_INIT(the_lock);					\
								tickets_threads_count = X;						\
								tickets = (struct ticket**)_liblock_allocate(sizeof(struct ticket*) * tickets_threads_count);	\
								for(int i=0; i<CFG.threads; i++){					\
									tickets[i] = (struct ticket*)_liblock_allocate(sizeof(struct ticket) * tickets_count);		\
									for(unsigned j=0; j<tickets_count; j++)		\
										tickets[i][j].val = i==0 ? 1 : 0;		\
								}

#define TM_SHUTDOWN()

#define USE_TM 		extern GL_T the_lock;				\
					extern struct ticket **tickets;				\
					extern volatile unsigned tickets_last_committed;		\
					extern unsigned tickets_count;				\
					extern unsigned tickets_backoff;			\
					extern unsigned tickets_threads_count;

# define TM_THREAD_ENTER() 	unsigned tid = thread_getId();							\
							ticket * my_tickets = tickets[tid];						\
							ticket * successor_tickets = tickets[(tid+1)%tickets_threads_count];	// FIXME Assume Here that next Thread executing next Transaction

# define TM_BEGIN(X)			{ 																\
									int tries = 5;  											\
									int age = X;  												\
									while (1) { 												\
										while (IS_GL_LOCKED(the_lock)) { __asm__ ( "pause;" ); } 	\
										int status = _xbegin(); 								\
										if (status == _XBEGIN_STARTED) {if (IS_GL_LOCKED(the_lock)) _xabort(25); break; } \
										if ((status & _XABORT_EXPLICIT) && _XABORT_CODE(status)==27) {					  \
											ordering_abort++;									\
										} else if ((status & _XABORT_EXPLICIT) && _XABORT_CODE(status)==25) {			  \
											gl_abort++;											\
										} else if (status & _XABORT_CAPACITY){					\
											capacity_abort++;									\
										} else if (status & _XABORT_CONFLICT){					\
											conflict_abort++;									\
										} else{													\
											other_abort++;}										\
										tries--;												\
										if (tries <= 0) {   									\
											while(my_tickets[tickets_count-1].val == 0);		\
											GL_LOCK(the_lock);			 						\
											break;  											\
										}   													\
									}

# define TM_END()					bool found = false;											\
									for(unsigned b=0; b<tickets_count; b++){					\
										if(my_tickets[b].val){	/* consume the tickets*/ 		\
											found = true;		/* my turn to commit */ 		\
											break;												\
										}														\
										for(unsigned i=0; i<tickets_backoff; i++)	/* tickets_backoff */ 		\
											__asm__ ( "nop;" );									\
									}															\
									if(!found){													\
										TM_ABORT(27);	/* abort and retry */					\
									}															\
									/*COMMIT-TX*/ 												\
									if (tries > 0) {    										\
										_xend();    											\
										htm_count++;											\
									} else {    												\
										GL_UNLOCK(the_lock);			    					\
										gl_count++;												\
									}															\
									tickets_last_committed = age;								\
									/* wait till the previous thread completes setting all my tickets */	\
									while(my_tickets[0].val == 0);								\
									/* reset my tickets */										\
									for(unsigned j=0; j<tickets_count; j++){					\
										my_tickets[j].val = 0;									\
									}															\
									/* set the next Tx tickets */								\
									for(int b=tickets_count-1; b>=0; b--){						\
										/* set the ticket */									\
										successor_tickets[b].val = 1;							\
										/* check if other thread got notified and committed */	\
										if(tickets_last_committed != age+1){					\
											/* tickets_backoff */								\
											for(unsigned i=0; i<tickets_backoff * 2; i++)		\
												__asm__ ( "nop;" );	/* wait */ 					\
										}														\
									} \
								};

# define TM_ABORT(X)		_xabort(X);


#define TM_THREAD_EXIT()

#endif
