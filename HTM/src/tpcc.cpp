//============================================================================
// Name        : HTM.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <stdlib.h>
#include <string.h>
#include <iostream>
#include <stdio.h>
#include <pthread.h>
#include <sys/time.h>
using namespace std;

#include "bench.hpp"
#include "api_htmgl.h"
#include "thread.h"
#include "stats.hpp"
#include "trace.hpp"
#include "tpcc.h"

USE_TM

#ifdef OCTONAUTS
#include "octonauts.h"
USE_OCTONAUTS
#endif

namespace tpcc{


void init() {
	printf("TPC-C\n");
}

unsigned long long get_real_time() {
	struct timespec time;
	clock_gettime(CLOCK_MONOTONIC_RAW, &time);
	return time.tv_sec * 1000000000L + time.tv_nsec;
}

void run(void* p) {
	int processing = CFG.oprations;
	TM_THREAD_ENTER()
	STATS_START
	int thread_id = thread_getId();
	int _age = thread_id;
	int loop = CFG.iterations/CFG.threads;
	unsigned int seed = (unsigned int) get_real_time();
	for(int i=0; i<loop; i++){
		uint32_t val = rand_r_32(&seed) % 256;
		uint32_t act = rand_r_32(&seed) % 100;
		int w_id = rand_r_32(&seed) % (NUM_WAREHOUSES);
		int o_id = rand_r_32(&seed) % (NUM_ORDERS_PER_D);
		int c_id = rand_r_32(&seed) % (NUM_CUSTOMERS_PER_D);
		int d_id = rand_r_32(&seed) % (NUM_DISTRICTS);
		int l_id = rand_r_32(&seed) % (NUM_LINES_PER_O);
		int orderLineCount = rand_r_32(&seed) % NUM_LINES_PER_O;
		int crtdate = rand_r_32(&seed) % 50000;
		float h_amount = (float) (rand_r_32(&seed) % (500000) * 0.01);
		// start the transaction
#ifdef OCTONAUTS
		OCTONAUTS_BEGIN(w_id)
#endif
		TM_BEGIN(_age)
		for(int p=0; p<processing; p++){
			// do some transactional work
			if (act < 4) {
				//		(READ_ONLY_TX);
				//		(TX_ORDER_STATUS);
				float dummy;
				dummy = TM_SHARED_READ(warehouses[w_id].W_YTD);
				dummy = TM_SHARED_READ(orders[w_id][o_id].O_CARRIER_ID);
				dummy = TM_SHARED_READ(customers[w_id][d_id][c_id].C_BALANCE);

				float olsum = (float) 0;
				int i = 1;

				while (i < orderLineCount) {
					olsum += TM_SHARED_READ(order_lines[w_id][o_id][i].OL_AMOUNT);
					dummy = TM_SHARED_READ(order_lines[w_id][o_id][i].OL_QUANTITY);
					i += 1;
				}
			} else if (act < 8) {
				//		(READ_ONLY_TX);
				//		(TX_STOCKLEVEL);
				//TODO I believe this method is wrong!!
				double dummy;

				dummy = TM_SHARED_READ(warehouses[w_id].W_YTD);

				dummy = TM_SHARED_READ(orders[w_id][o_id].O_CARRIER_ID);

				dummy = TM_SHARED_READ(order_lines[w_id][o_id][l_id].OL_QUANTITY);

				int k = 1;
				while (k <= 10) {
					int s_id = rand_r_32(&seed) % (NUM_ITEMS);
					dummy = TM_SHARED_READ(stocks[w_id][s_id].S_QUANTITY);

					k += 1;
				}
			} else if (act < 12) {
				//		(READ_WRITE_TX);
				//		(TX_DELIVERY);
				double dummy;
				for (int d_id = 0; d_id < NUM_DISTRICTS; d_id++) {
					dummy = TM_SHARED_READ(warehouses[w_id].W_YTD);
					dummy = TM_SHARED_READ(orders[w_id][o_id].O_CARRIER_ID);

					float olsum = (float) 0;

					int i = 1;
					while (i < orderLineCount) {
						olsum += TM_SHARED_READ(
								order_lines[w_id][o_id][i].OL_AMOUNT);
						dummy = TM_SHARED_READ(
								order_lines[w_id][o_id][i].OL_QUANTITY);
						i += 1;
					}
					TM_SHARED_WRITE(customers[w_id][d_id][c_id].C_BALANCE,
							TM_SHARED_READ(customers[w_id][d_id][c_id].C_BALANCE) + olsum);
					TM_SHARED_WRITE(customers[w_id][d_id][c_id].C_DELIVERY_CNT,
							TM_SHARED_READ(customers[w_id][d_id][c_id].C_DELIVERY_CNT) + 1);
				}

			} else if (act < 55) {
				//		(READ_WRITE_TX);
				//		(TX_PAYMENT);

				// Open Wairehouse Table

				TM_SHARED_WRITE(warehouses[w_id].W_YTD,
						TM_SHARED_READ(warehouses[w_id].W_YTD) + h_amount);

				// In DISTRICT table

				TM_SHARED_WRITE(districts[w_id][d_id].D_YTD,
						TM_SHARED_READ(districts[w_id][d_id].D_YTD) + h_amount);

				TM_SHARED_WRITE(customers[w_id][d_id][c_id].C_BALANCE,
						TM_SHARED_READ(customers[w_id][d_id][c_id].C_BALANCE) - h_amount);
				TM_SHARED_WRITE(customers[w_id][d_id][c_id].C_YTD_PAYMENT,
						TM_SHARED_READ(customers[w_id][d_id][c_id].C_YTD_PAYMENT) + h_amount);
				TM_SHARED_WRITE(customers[w_id][d_id][c_id].C_PAYMENT_CNT,
						TM_SHARED_READ(customers[w_id][d_id][c_id].C_PAYMENT_CNT) + 1);
			} else {
				//			(READ_WRITE_TX);
				//			(TX_NEWORDER);

				double D_TAX = TM_SHARED_READ(districts[w_id][d_id].D_TAX);
				int o_id = TM_SHARED_READ(districts[w_id][d_id].D_NEXT_O_ID)%(NUM_ORDERS_PER_D);
				TM_SHARED_WRITE(districts[w_id][d_id].D_NEXT_O_ID, o_id + 1);

				double C_DISCOUNT = TM_SHARED_READ(
						customers[w_id][d_id][c_id].C_DISCOUNT);
				int C_LAST = TM_SHARED_READ(customers[w_id][d_id][c_id].C_LAST);
				int C_CREDIT = TM_SHARED_READ(
						customers[w_id][d_id][c_id].C_CREDIT);

				//TODO we don't have create, so simulate it with one write in the array in random index
				// Create entries in ORDER and NEW-ORDER

				//			order_t  norder;// = (order_t*)TM_ALLOC(sizeof(order_t));
				//			norder.O_C_ID = c_id;
				//			norder.O_CARRIER_ID = rand_r_32(seed) %(10); //1000
				//			norder.O_ALL_LOCAL = true;

				TM_SHARED_WRITE(orders[w_id][o_id].O_CARRIER_ID,
						rand_r_32(&seed) % (10));

				int items_count = 1 + rand_r_32(&seed) % (5);

				for (int i = 0; i <= items_count; i++) {
					int i_id = rand_r_32(&seed) % (NUM_ITEMS);

					float I_PRICE = TM_SHARED_READ(items[i_id].I_PRICE);
					int I_NAME = TM_SHARED_READ(items[i_id].I_NAME);
					int I_DATA = TM_SHARED_READ(items[i_id].I_DATA);

					//order_line_t orderLine;// = (order_line_t*)TM_ALLOC(sizeof(order_line_t));

					int l_id = rand_r_32(&seed) % (NUM_LINES_PER_O);

					//				orderLine.OL_QUANTITY = rand_r_32(seed) %(1000);
					//				orderLine.OL_I_ID = i_id;
					//				orderLine.OL_SUPPLY_W_ID = w_id;
					//				orderLine.OL_AMOUNT = (int)(orderLine.OL_QUANTITY  * I_PRICE);
					//				orderLine.OL_DELIVERY_D = 0;
					//				orderLine.OL_DIST_INFO = d_id;

					TM_SHARED_WRITE(order_lines[w_id][o_id][l_id].OL_QUANTITY,
							rand_r_32(&seed) % (1000));

				}

			}
		}
		TM_END()
#ifdef OCTONAUTS
		OCTONAUTS_END(w_id)
#endif
		_age += CFG.threads; // increment the age
	}
	STATS_END
	TM_THREAD_EXIT()
}

void destroy() {
}

}
