#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>

#include "bench.hpp"
#include "rbtree.h"
#include "thread.h"
#include "stats.hpp"
#include "trace.hpp"

typedef rbtree_t intset_t;

USE_TM

namespace redblacktree{

intset_t *set;
struct trace* trace;
struct trace** traces;


void init() {
	trace = build_trace(1983, CFG.iterations);
	traces = split_trace(trace, CFG.threads);
	set = rbtree_alloc(NULL);
	printf("RedBlack Tree of %d range, %d initial entries, and %d update\n", CFG.range, CFG.warm, CFG.update);
	for (int i = 0; i < CFG.warm; i++) {
		rbtree_insert(set, (void*) (i%CFG.range), (void*) (i%CFG.range));
	}
}

void destroy() {
	rbtree_free(set);
}

void run(void* p) {
	int processing = CFG.oprations;
	TM_THREAD_ENTER()
	struct trace* local_trace = traces[(unsigned)thread_getId()];
	int update = CFG.update;
	int range = CFG.range;
	STATS_START
	for (int i = 0; i < local_trace->length; i++) {
		struct trace_element e = local_trace->elements[i];
		int op = e.arg1 % 100;
		int val = e.arg2 % range;
		if (op < update) {
			if (op < update / 2) {
				/* Add random value */
				TM_BEGIN(e.age);
					for(int p=0; p<processing; p++)
						TMrbtree_insert(TM_ARG set, (void*) val, (void*) val);
				TM_END();
			} else {
				/* Remove random value */
				TM_BEGIN(e.age);
					for(int p=0; p<processing; p++)
						TMrbtree_delete(TM_ARG set, (void*) val);
				TM_END();
			}
		} else {
			/* Look for random value */
			TM_BEGIN(e.age);
				for(int p=0; p<processing; p++)
					TMrbtree_contains(TM_ARG set, (void*) val);
			TM_END();
		}
	}
	STATS_END
	TM_THREAD_EXIT()
}

}
