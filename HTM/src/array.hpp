
#ifndef ARRAY_HPP_
#define ARRAY_HPP_

namespace array{
	void init();
	void destroy();
	void run(void* tid);
}

#endif
