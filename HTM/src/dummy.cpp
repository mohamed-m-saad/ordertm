//============================================================================
// Name        : HTM.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <stdlib.h>
#include <string.h>
#include <iostream>
#include <stdio.h>
#include <pthread.h>
#include <sys/time.h>
using namespace std;

#include "bench.hpp"
#include "api_htmgl.h"
#include "thread.h"
#include "stats.hpp"
#include "trace.hpp"

USE_TM

namespace dummy{

void init() {
//	tickets_backoff = CFG.update;
//	tickets_count = CFG.range;
	printf("Dummy\n", CFG.oprations);
}

void run(void* p) {
	int loop = CFG.iterations/CFG.threads;
	int processing = CFG.oprations;
	TM_THREAD_ENTER()
	STATS_START
	int thread_id = thread_getId();
	int _age = thread_id;
	for(int i=0; i<loop; i++){
		// start the transaction
		TM_BEGIN(_age)
		// do some transactional work
		for(int p=0; p<processing; p++)
			__asm__ ( "nop;" );
		TM_END()
		_age += CFG.threads; // increment the age
	}
	STATS_END
	TM_THREAD_EXIT()
}

void destroy() {
}

}
