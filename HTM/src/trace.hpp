/*
 * trace.hpp
 *
 *  Created on: Dec 8, 2015
 *      Author: root
 */

#ifndef TRACE_HPP_
#define TRACE_HPP_

struct trace_element{
	int age;
	int arg1;
	int arg2;
	int arg3;
	int arg4;
	int arg5;
};

struct trace{
	struct trace_element* elements;
	int length;

	trace(int l){
		length = l;
		elements = new struct trace_element[length];
	}
};

struct trace* build_trace(int seed, int length);

struct trace** split_trace(struct trace* origin, int nodes);

#endif /* TRACE_HPP_ */
