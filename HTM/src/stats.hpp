/*
 * stats.hpp
 *
 *  Created on: Dec 5, 2015
 *      Author: root
 */

#ifndef STATS_HPP_
#define STATS_HPP_

struct stats{
	int count_ordering_abort;
	int count_capacity_abort;
	int count_conflict_abort;
	int count_other_abort;
	int count_gl_abort;
	int count_gl_count;
	int count_htm_count;
};

#define STATS_START		int ordering_abort=0, capacity_abort=0, conflict_abort=0, other_abort=0, gl_abort=0, gl_count=0, htm_count=0;
#define STATS_END		struct stats* s = thread_getStatistics(); s->count_ordering_abort += ordering_abort; s->count_capacity_abort += capacity_abort; s->count_conflict_abort += conflict_abort; s->count_other_abort += other_abort; s->count_gl_abort += gl_abort; s->count_gl_count += gl_count; s->count_htm_count += htm_count;

#endif /* STATS_HPP_ */
