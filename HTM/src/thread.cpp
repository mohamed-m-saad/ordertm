/* =============================================================================
 *
 * thread.c
 *
 * =============================================================================
 *
 * Copyright (C) Stanford University, 2006.  All Rights Reserved.
 * Author: Chi Cao Minh
 *
 * =============================================================================
 */


#include <assert.h>
#include <stdlib.h>
#include <stdio.h>
#include "thread.h"
#include "types.h"
#include "stats.hpp"

struct stats*	 statistics;
static THREAD_LOCAL_T    global_threadId;
static long              global_dummyThread     = 1;
static long              global_numThread       = 1;
static THREAD_BARRIER_T* global_barrierPtr      = NULL;
static long*             global_threadIds       = NULL;
static THREAD_ATTR_T     global_threadAttr;
static THREAD_T*         global_threads         = NULL;
static void            (*global_funcPtr)(void*) = NULL;
static void*             global_argPtr          = NULL;
static volatile bool_t   global_doShutdown      = FALSE;


/* =============================================================================
 * threadWait
 * -- Synchronizes all threads to start/stop parallel section
 * =============================================================================
 */
static void
threadWait (void* argPtr)
{
    long threadId = *(long*)argPtr;

	cpu_set_t new_mask;
	cpu_set_t updated_mask;
	cpu_set_t was_mask;
//	int tid = (int)(threadId<4 ? threadId * 2 : threadId - 3);//omp_get_thread_num();
	int tid = (int)threadId;
	CPU_ZERO(&new_mask);
	CPU_SET(tid, &new_mask);
	if (sched_getaffinity(0, sizeof(was_mask), &was_mask) == -1)
		printf("Error: sched_getaffinity(%d, sizeof(was_mask), &was_mask)\n", tid);
	if (sched_setaffinity(0, sizeof(new_mask), &new_mask) == -1)
		printf("Error: sched_setaffinity(%d, sizeof(new_mask), &new_mask)\n", tid);
	if (sched_getaffinity(0, sizeof(updated_mask), &updated_mask) == -1)
		printf("Error: sched_getaffinity(%d, sizeof(updated_mask), &updated_mask)\n", tid);
	printf("Affinity tid=%d new_mask=%08X old_mask=%08X\n", tid, *(unsigned int*)(&updated_mask), *(unsigned int*)(&was_mask));

    THREAD_LOCAL_SET(global_threadId, (long)threadId);

    while (1) {
        THREAD_BARRIER(global_barrierPtr, threadId); /* wait for start parallel */
        if (global_doShutdown) {
            break;
        }
        if(threadId < global_dummyThread)
        	global_funcPtr(global_argPtr);
        THREAD_BARRIER(global_barrierPtr, threadId); /* wait for end parallel */
        if (threadId == 0) {
            break;
        }
    }
}


/* =============================================================================
 * thread_startup
 * -- Create pool of secondary threads
 * -- numThread is total number of threads (primary + secondaries)
 * =============================================================================
 */
void
thread_startup (long numThread)
{
    long i;

    global_dummyThread = numThread;
    while((numThread & (numThread - 1)) != 0)
    	numThread++;

    global_numThread = numThread;
    global_doShutdown = FALSE;

    /* Set up barrier */
    assert(global_barrierPtr == NULL);
    global_barrierPtr = THREAD_BARRIER_ALLOC(numThread);
    assert(global_barrierPtr);
    THREAD_BARRIER_INIT(global_barrierPtr, numThread);

    /* Set up ids */
    statistics = new struct stats[numThread];
	for(int i=0; i<numThread; i++){
		statistics[i].count_ordering_abort = statistics[i].count_capacity_abort = statistics[i].count_conflict_abort = statistics[i].count_gl_abort = statistics[i].count_gl_count = statistics[i].count_htm_count = statistics[i].count_other_abort = 0;
	}
    THREAD_LOCAL_INIT(global_threadId);
    assert(global_threadIds == NULL);
    global_threadIds = (long*)malloc(numThread * sizeof(long));
    assert(global_threadIds);
    for (i = 0; i < numThread; i++) {
        global_threadIds[i] = i;
    }

    /* Set up thread list */
    assert(global_threads == NULL);
    global_threads = (THREAD_T*)malloc(numThread * sizeof(THREAD_T));
    assert(global_threads);

    /* Set up pool */
    THREAD_ATTR_INIT(global_threadAttr);
    for (i = 1; i < numThread; i++) {
        THREAD_CREATE(global_threads[i],
                      global_threadAttr,
                      &threadWait,
                      &global_threadIds[i]);
    }

    /*
     * Wait for primary thread to call thread_start
     */
}


/* =============================================================================
 * thread_start
 * -- Make primary and secondary threads execute work
 * -- Should only be called by primary thread
 * -- funcPtr takes one arguments: argPtr
 * =============================================================================
 */
void
thread_start (void (*funcPtr)(void*), void* argPtr)
{
    global_funcPtr = funcPtr;
    global_argPtr = argPtr;

    long threadId = 0; /* primary */
    threadWait((void*)&threadId);
}


/* =============================================================================
 * thread_shutdown
 * -- Primary thread kills pool of secondary threads
 * =============================================================================
 */
void
thread_shutdown ()
{
    /* Make secondary threads exit wait() */
    global_doShutdown = TRUE;
    THREAD_BARRIER(global_barrierPtr, 0);

    long numThread = global_numThread;

    long i;
    for (i = 1; i < numThread; i++) {
        THREAD_JOIN(global_threads[i]);
    }

    THREAD_BARRIER_FREE(global_barrierPtr);
    global_barrierPtr = NULL;

    free(global_threadIds);
    global_threadIds = NULL;

    free(global_threads);
    global_threads = NULL;

    global_numThread = 1;
}


/* =============================================================================
 * thread_barrier_alloc
 * =============================================================================
 */
thread_barrier_t*
thread_barrier_alloc (long numThread)
{
    thread_barrier_t* barrierPtr;

    assert(numThread > 0);
    assert((numThread & (numThread - 1)) == 0); /* must be power of 2 */
    barrierPtr = (thread_barrier_t*)malloc(numThread * sizeof(thread_barrier_t));
    if (barrierPtr != NULL) {
        barrierPtr->numThread = numThread;
    }

    return barrierPtr;
}


/* =============================================================================
 * thread_barrier_free
 * =============================================================================
 */
void
thread_barrier_free (thread_barrier_t* barrierPtr)
{
    free(barrierPtr);
}


/* =============================================================================
 * thread_barrier_init
 * =============================================================================
 */
void
thread_barrier_init (thread_barrier_t* barrierPtr)
{
    long i;
    long numThread = barrierPtr->numThread;

    for (i = 0; i < numThread; i++) {
        barrierPtr[i].count = 0;
        THREAD_MUTEX_INIT(barrierPtr[i].countLock);
        THREAD_COND_INIT(barrierPtr[i].proceedCond);
        THREAD_COND_INIT(barrierPtr[i].proceedAllCond);
    }
}


/* =============================================================================
 * thread_barrier
 * -- Simple logarithmic barrier
 * =============================================================================
 */
void
thread_barrier (thread_barrier_t* barrierPtr, long threadId)
{
    long i = 2;
    long base = 0;
    long index;
    long numThread = barrierPtr->numThread;

    if (numThread < 2) {
        return;
    }

    do {
        index = base + threadId / i;
        if ((threadId % i) == 0) {
            THREAD_MUTEX_LOCK(barrierPtr[index].countLock);
            barrierPtr[index].count++;
            while (barrierPtr[index].count < 2) {
                THREAD_COND_WAIT(barrierPtr[index].proceedCond,
                                 barrierPtr[index].countLock);
            }
            THREAD_MUTEX_UNLOCK(barrierPtr[index].countLock);
        } else {
            THREAD_MUTEX_LOCK(barrierPtr[index].countLock);
            barrierPtr[index].count++;
            if (barrierPtr[index].count == 2) {
                THREAD_COND_SIGNAL(barrierPtr[index].proceedCond);
            }
            while (THREAD_COND_WAIT(barrierPtr[index].proceedAllCond,
                                    barrierPtr[index].countLock) != 0)
            {
                /* wait */
            }
            THREAD_MUTEX_UNLOCK(barrierPtr[index].countLock);
            break;
        }
        base = base + numThread / i;
        i *= 2;
    } while (i <= numThread);

    for (i /= 2; i > 1; i /= 2) {
        base = base - numThread / i;
        index = base + threadId / i;
        THREAD_MUTEX_LOCK(barrierPtr[index].countLock);
        barrierPtr[index].count = 0;
        THREAD_COND_SIGNAL(barrierPtr[index].proceedAllCond);
        THREAD_MUTEX_UNLOCK(barrierPtr[index].countLock);
    }
}


/* =============================================================================
 * thread_getId
 * -- Call after thread_start() to get thread ID inside parallel region
 * =============================================================================
 */
long
thread_getId()
{
    return (long)THREAD_LOCAL_GET(global_threadId);
}


/* =============================================================================
 * thread_getNumThread
 * -- Call after thread_start() to get number of threads inside parallel region
 * =============================================================================
 */
long
thread_getNumThread()
{
    return global_numThread;
}


/* =============================================================================
 * thread_barrier_wait
 * -- Call after thread_start() to synchronize threads inside parallel region
 * =============================================================================
 */
void
thread_barrier_wait()
{
#ifndef SIMULATOR
    long threadId = thread_getId();
#endif /* !SIMULATOR */
    THREAD_BARRIER(global_barrierPtr, threadId);
}

/* =============================================================================
 * statistics
 * =============================================================================
 */

struct stats*
thread_getStatistics()
{
	int tid = (long)THREAD_LOCAL_GET(global_threadId);
    return &statistics[tid];
}

void
thread_printStatistics(){
	int count_ordering_abort=0, count_capacity_abort=0, count_conflict_abort=0, count_other_abort=0, count_gl_abort=0, count_gl=0, count_htm=0;
	for(int i=0; i<global_numThread; i++){
		int all_aborts = statistics[i].count_ordering_abort + statistics[i].count_capacity_abort + statistics[i].count_conflict_abort + statistics[i].count_other_abort + statistics[i].count_gl_abort;
		printf("%d: ordering_abort=%d, capacity_abort=%d, conflict_abort=%d, other_abort=%d, gl_abort=%d, all_abort_count=%d, gl_count=%d, htm_count=%d\n",
				i,
				statistics[i].count_ordering_abort,
				statistics[i].count_capacity_abort,
				statistics[i].count_conflict_abort,
				statistics[i].count_other_abort,
				statistics[i].count_gl_abort,
				all_aborts,
				statistics[i].count_gl_count,
				statistics[i].count_htm_count);
		count_ordering_abort += statistics[i].count_ordering_abort;
		count_capacity_abort += statistics[i].count_capacity_abort;
		count_conflict_abort+=statistics[i].count_conflict_abort;
		count_other_abort+=statistics[i].count_other_abort;
		count_gl_abort+=statistics[i].count_gl_abort;
		count_gl+=statistics[i].count_gl_count;
		count_htm+=statistics[i].count_htm_count;
	}
	int total_tx = count_htm + count_gl;
	int total_aborts = count_ordering_abort + count_capacity_abort + count_conflict_abort + count_other_abort + count_gl_abort;
	int total_aborts_temp = total_aborts;
	if(total_aborts==0)
		total_aborts = 1; // avoid division by zero error

	printf("Total: ordering_abort=%d, capacity_abort=%d, conflict_abort=%d, other_abort=%d, gl_abort=%d, all_abort_count=%d, gl_count=%d, htm_count=%d\n",
			count_ordering_abort,
			count_capacity_abort,
			count_conflict_abort,
			count_other_abort,
			count_gl_abort,
			total_aborts_temp,
			count_gl,
			count_htm);
	printf("Ratio: ordering_abort=%2.1f, capacity_abort=%2.1f, conflict_abort=%2.1f, other_abort=%2.1f, gl_abort=%2.1f, all_abort_count=%2.1f, gl_count=%2.1f, htm_count=%2.1f\n",
			100.0 * count_ordering_abort / total_aborts,
			100.0 * count_capacity_abort / total_aborts,
			100.0 * count_conflict_abort / total_aborts,
			100.0 * count_other_abort / total_aborts,
			100.0 * count_gl_abort / total_aborts,
			100.0 * total_aborts_temp / total_tx,
			100.0 * count_gl / total_tx,
			100.0 * count_htm / total_tx);
}

/* =============================================================================
 * TEST_THREAD
 * =============================================================================
 */
#ifdef TEST_THREAD


#include <stdio.h>
#include <unistd.h>


#define NUM_THREADS    (4)
#define NUM_ITERATIONS (3)



void
printId (void* argPtr)
{
    long threadId = thread_getId();
    long numThread = thread_getNumThread();
    long i;

    for ( i = 0; i < NUM_ITERATIONS; i++ ) {
        thread_barrier_wait();
        if (threadId == 0) {
            sleep(1);
        } else if (threadId == numThread-1) {
            usleep(100);
        }
        printf("i = %li, tid = %li\n", i, threadId);
        if (threadId == 0) {
            puts("");
        }
        fflush(stdout);
    }
}


int
main ()
{
    puts("Starting...");

    /* Run in parallel */
    thread_startup(NUM_THREADS);
    /* Start timing here */
    thread_start(printId, NULL);
    thread_start(printId, NULL);
    thread_start(printId, NULL);
    /* Stop timing here */
    thread_shutdown();

    puts("Done.");

    return 0;
}


#endif /* TEST_THREAD */


/* =============================================================================
 *
 * End of thread.c
 *
 * =============================================================================
 */
