//============================================================================
// Name        : HTM.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <stdlib.h>
#include <string.h>
#include <iostream>
#include <stdio.h>
#include <pthread.h>
#include <sys/time.h>
using namespace std;

#include "bench.hpp"
#include "api_htmgl.h"
#include "thread.h"
#include "stats.hpp"
#include "trace.hpp"

USE_TM

namespace disjoint2{

#define CACHE_LINE_SIZE                   64

#define MEMALIGN(ptr, alignment, size)    posix_memalign((void **)(ptr),  (alignment), (size))
#define r_align(n, r)                     (((n) + (r) - 1) & -(r))
#define cache_align(n)                    r_align(n , CACHE_LINE_SIZE)
#define pad_to_cache_line(n)              (cache_align(n) - (n))

inline void* liblock_allocate(size_t n)
{
    void* res = 0;
    if((MEMALIGN(&res, CACHE_LINE_SIZE, cache_align(n)) < 0) || !res){
        printf("MEMALIGN(%llu, %llu)", (unsigned long long)n, (unsigned long long)cache_align(n));
    }
    return res;
}

struct cache_line2{
	unsigned char padding[CACHE_LINE_SIZE-1];
	unsigned char val;
};

struct cache_line2 **caches;

void init() {
	printf("Disjoint2 of %d entries per thread [entry size %d], pause time %d\n", CFG.range, sizeof(struct cache_line2), CFG.warm);
	caches = (struct cache_line2**)liblock_allocate(sizeof(struct cache_line2*) * CFG.threads);
	for(int i=0; i<CFG.threads; i++){
		caches[i] = (struct cache_line2*)liblock_allocate(sizeof(struct cache_line2) * CFG.range);
	}
}

void run(void* p) {
	int processing = CFG.oprations;
	int busy = CFG.warm;
	int loop = CFG.iterations;
	int caches_count = CFG.range;
	TM_THREAD_ENTER()
	STATS_START
	int _age = thread_getId();
	struct cache_line2* my_caches = caches[thread_getId()];
	for(int i=0; i<loop; i++){
		TM_BEGIN(_age)
		for(int p=0; p<processing; p++){
			for(int j=0; j<caches_count; j++)
				my_caches[j].val = (unsigned char)j+'a';
			for(int i=0; i<busy; i++)
				__asm__ ( "pause;" );
		}
		TM_END()
		_age += CFG.threads;
	}
	STATS_END
	TM_THREAD_EXIT()
}

void destroy() {
	for(int i=0; i<CFG.threads; i++){
		struct cache_line2 * my_caches = caches[i];
		printf("Thread %d: ", i);
		for(int j=0; j<CFG.range; j++)
			printf(" %p[%c]", &(my_caches[j].val), my_caches[j].val);
		printf("\n");
	}
}

}
