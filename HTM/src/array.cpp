//============================================================================
// Name        : HTM.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <stdlib.h>
#include <string.h>
#include <iostream>
#include <stdio.h>
#include <pthread.h>
#include <sys/time.h>
using namespace std;

#include "bench.hpp"
#include "api_htmgl.h"
#include "thread.h"
#include "stats.hpp"
#include "trace.hpp"

USE_TM

namespace array{

struct trace* trace;
struct trace** traces;

long *data;

void init() {
	trace = build_trace(1983, CFG.iterations);
	traces = split_trace(trace, CFG.threads);
	printf("Array of %d entries\n", CFG.range);
	data = new long[CFG.range];
}

void run(void* p) {
	int processing = CFG.oprations;
	TM_THREAD_ENTER()
	struct trace* local_trace = traces[thread_getId()];
	int range = CFG.range;
	STATS_START
	for (int i = 0; i < local_trace->length; i++) {
		struct trace_element e = local_trace->elements[i];
		TM_BEGIN(e.age)
			for(int p=0; p<processing; p++){
				data[e.arg1 % range] = e.arg2;
				data[e.arg2 % range] = e.arg3;
				data[e.arg3 % range] = e.arg4;
				data[e.arg4 % range] = e.arg5;
				data[e.arg5 % range] = e.arg1;
			}
		TM_END()
	}
	STATS_END
	TM_THREAD_EXIT()
}

void destroy() {
}

}
