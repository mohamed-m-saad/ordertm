/*
 * octonauts.h
 *
 *  Created on: Jan 3, 2016
 *      Author: root
 */

#ifndef OCTONAUTS_H_
#define OCTONAUTS_H_

#include <stdint.h>

#define Q_COUNTS 50
#define CACHELINE_BYTES 64

struct pad_word_t {
	volatile uintptr_t val;
	volatile uintptr_t val2;
	char pad[CACHELINE_BYTES - (2 * sizeof(uintptr_t))];
};

#define USE_OCTONAUTS 					extern bool withYield;	\
										extern pad_word_t enq_counter[Q_COUNTS];	\
										extern pad_word_t deq_counter[Q_COUNTS];

#define OCTONAUTS_BEGIN(X)				uintptr_t ticket = __sync_fetch_and_add(&enq_counter[X].val, 1);	\
										while (deq_counter[X].val != ticket) if (withYield)	sched_yield();

#define OCTONAUTS_END(X)				__sync_fetch_and_add(&deq_counter[X].val, 1);

#endif /* OCTONAUTS_H_ */
