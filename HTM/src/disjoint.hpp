
#ifndef DISJOINT_HPP_
#define DISJOINT_HPP_

namespace disjoint{
	void init();
	void destroy();
	void run(void* tid);
}

#endif
