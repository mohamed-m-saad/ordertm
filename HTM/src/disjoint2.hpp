
#ifndef DISJOINT2_HPP_
#define DISJOINT2_HPP_

namespace disjoint2{
	void init();
	void destroy();
	void run(void* tid);
}

#endif
