#Supporting Transactional Memory Commit Order#

In this work, we propose an effective solution to address the problem of committing transactions enforcing a pre-defined order. To do that, we provide the design and implementation of two algorithms, OUL and OWB, which deploy a co-operative transaction execution that circumvents the transaction isolation constraint in favor of propagating written values among conflicting transactions. 

A work-in-progress is ordered HTM. In this work we provide two algorithms: ticketing and timeline that support commitment ordering for Intel's Haswell chips using RTX.